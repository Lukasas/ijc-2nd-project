/*
	#FILENAME: 		tail.c
	#DESCRIPTION:	Vytiskne posledních/od X řádky/ů
	#Author:		Lukáš Chábek
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>
#define BUFFER_SIZE 510
#define _MY_DEBUG_
#undef _MY_DEBUG_

void ReadFromLine(FILE *f, unsigned int read_from)
{	
	char buffer[BUFFER_SIZE+1];
	unsigned int err_print = 0;
	unsigned int printed_data = 0;
	unsigned int line_counter = 1;
	while (fgets(buffer, BUFFER_SIZE+1, f) != 0)
	{
		if (!printed_data++)
		{
			if (buffer[strlen(buffer)-1] != '\n')
			{
				if (!err_print++)
					fprintf(stderr, "Byla překročena maximální délka řádku %d.\n", BUFFER_SIZE);
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
				if (line_counter >= read_from)
					printf("%s\n", buffer);
			}
			else
			{
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
			if (line_counter >= read_from)
				printf("%s", buffer);
			}
		}

		if (buffer[strlen(buffer)-1] == '\n')
		{
			printed_data = 0;
			line_counter++;
		}
	}
}
void ReadFromLineEntering(FILE *f, unsigned int read_from)
{	
	char buffer[BUFFER_SIZE+1];
	unsigned int err_print = 0;
	unsigned int printed_data = 0;
	unsigned int line_counter = 1;
	while (fgets(buffer, BUFFER_SIZE+1, f) != 0)
	{
		if (!printed_data++)
		{
			if (buffer[strlen(buffer)-1] != '\n')
			{
				if (!err_print++)
					fprintf(stderr, "Byla překročena maximální délka řádku %d.\n", BUFFER_SIZE);
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
				if (line_counter >= read_from)
					printf("%s\n", buffer);
			}
			else
			{
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
			if (line_counter >= read_from)
				printf("%s", buffer);
			}
		}

		if (buffer[strlen(buffer)-1] == '\n')
		{
			printed_data = 0;
			line_counter++;
		}
	}
}

void ReadEverything(FILE *f, int lines_to_read)
{

	char buffer[BUFFER_SIZE+1];
	char c = NULL;	
	fseek(f, 0, SEEK_END);
	int line_counterm = 0;
	int totalback = 0;
	while (line_counterm != lines_to_read)
	{
		while (c != '\n')
		{
			fseek(f, -2, SEEK_CUR);
			c = fgetc(f);
			//printf("ftell(f) = %d\n", ftell(f));
			if (ftell(f) == 1)
			{
				c = 0;
				fseek(f, 0, SEEK_SET);
				break;
			}
		}
		line_counterm++;
	
		fgets(buffer, BUFFER_SIZE+1, f);
		totalback = strlen(buffer);
/*#ifdef _MY_DEBUG_
		printf("%d ", line_counterm);
#endif
		if(buffer[totalback-1] != '\n')
			printf("%s\n", buffer);
		else		
			printf("%s", buffer);
			*/
		fseek(f, -totalback, SEEK_CUR);
		if (c == (char)0)
			break;
		c = (char)0;
	}
//	printf("%c", getc(f));
	
	//printf("%c", getc(f));
	
	//return 0;
	
	
	unsigned int err_print = 0;
	unsigned int printed_data = 0;
	unsigned int line_counter = 1;
	while (fgets(buffer, BUFFER_SIZE+1, f) != 0)
	{
		if (!printed_data)
		{
			printed_data++;
			if (buffer[strlen(buffer)-1] != '\n')
			{
				if (!err_print)
				{
					fprintf(stderr, "Byla překročena maximální délka řádku %d.\n", BUFFER_SIZE);
					err_print++;
				}
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
				//if (line_counter >= read_from)	
					printf("%s\n", buffer);
			}
			else
			{
#ifdef _MY_DEBUG_
				printf("%d-", line_counter);
#endif
				//if (line_counter >= read_from)	
					printf("%s", buffer);
			}
		}

		if (buffer[strlen(buffer)-1] == '\n')
		{
			printed_data = 0;
			line_counter++;
		}
	}
}


int main(int argc, char *argv[])
{

	char c;
	char * a = NULL;
	int nvalue = 0;
	FILE *f = NULL;
	
	opterr = 0;
	/*
	if (argc == 1)
	{
		fseek(stdin, 0, SEEK_END);
		if (ftell(stdin) != -1)
		{
			fseek(stdin, 0, SEEK_SET);
			
			ReadEverything(stdin, 10);		
			return 0;
		}
		fprintf(stderr, "Použití: tail [-n] [+][cislo] soubor.\n");
		return 2;
	}
	*/

	while ((c = getopt(argc, argv, "n:")) != -1)
	{
		switch (c)
		{
			case 'n':
			a = optarg;
			nvalue = 1;
			break;
			case '?':
				if (optopt == 'n')
				{
					fprintf(stderr, "Použití: tail [-n] [+][cislo] soubor.\n");
					return 2;
				}
			break;
		}
	}
	

	for (int i = optind; i < argc; i++)
	{
		f = fopen(argv[i], "r");
		if (f == NULL)
		{
			fprintf(stderr, "Neznámý argument %s\n", argv[i]);
			return 2;
		}
		else
			break;	
	}
	if (!f)
	{
		f = stdin;
		fseek(f, 0, SEEK_END);
		if (ftell(f) == -1)
		{
			if (nvalue == 1)
			{
				if (a[0] == '+')
					ReadFromLineEntering(f, abs(atoi(a+1)));
			}
			return 0;
		}
		fseek(f, 0, SEEK_SET);
	}
	
	if (nvalue == 1)
	{
		if (a[0] == '+')
			ReadFromLine(f, abs(atoi(a+1)));
		else
			ReadEverything(f, abs(atoi(a)));
	}
	else
		ReadEverything(f, 10);
		
	fclose(f);
	return 0;
}
