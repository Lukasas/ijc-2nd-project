/*
	#FILENAME: 		hash_function.c
	#DESCRIPTION:	Hashovací funkce, vrací index v poli
	#Author:		Lukáš Chábek
*/
#include "htable.h"

unsigned int hash_function(const char *str, unsigned htab_size)
{
	unsigned int h=0;
	const unsigned char *p;
	for(p=(const unsigned char*)str; *p!='\0'; p++)
		h = 65599*h + *p;
	return h % htab_size;
}
