/*
	#FILENAME: 		fgetw.h
	#DESCRIPTION:	Hlavička funkce fgetw 
	#Author:		Lukáš Chábek
*/
#ifndef _FGETW_IJC_
#define _FGETW_IJC_
#include <stdio.h>
#include <ctype.h>
int fgetw(char *s, int max, FILE *f);

#endif
