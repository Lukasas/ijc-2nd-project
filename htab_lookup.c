/*
	#FILENAME: 		htab_lookup.c
	#DESCRIPTION:	Přičtení / přidání položky do seznamu.
	#Author:		Lukáš Chábek
*/
#include "htable.h"

struct htab_listitem * htab_lookup(htab_t *t, const char *key)
{
	unsigned int index = hash_function(key, t->htab_size);
	struct htab_listitem *helper = t->ptr[index];
	struct htab_listitem *item = NULL;
	// Key doesn't exist
	if (helper == NULL)
	{
		helper = t->ptr[index] = hlist_create(key);
		if (helper == NULL)
			return NULL;
		strcpy(helper->key, key);
		return helper;
	}
	else
	{
		while (!helper)
		{
			if (strcmp(helper->key, key) == 0)
			{
				item = helper;
				break;
			}
			else
			{
				item = helper;
				helper = helper->next;
			}
		}
		
		if (item != NULL && helper != item)
		{
			helper = hlist_create(key);
			if (helper == NULL) return NULL;
			item->next = helper;
		}
	}
	
	return helper;	
}
