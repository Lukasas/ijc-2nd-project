/*
	#FILENAME: 		htab_clear.c
	#DESCRIPTION:	Smazání seznamu
	#Author:		Lukáš Chábek
*/
#include "htable.h"

void htab_clear(htab_t *t)
{
	struct htab_listitem *helper, *item;
	
	for (unsigned int i = 0; i < t->htab_size; i++)
	{
		if (!t->ptr[i]) continue;
		helper = item = t->ptr[i];
		while (helper != NULL)
		{
			helper = item->next;
			free(item);
			item = helper;
		}			
	}
	 
}
