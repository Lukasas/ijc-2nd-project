/*
	#FILENAME: 		htab_foreach.c
	#DESCRIPTION:	Funkce pro každý prvek v seznamu.
	#Author:		Lukáš Chábek
*/
#include "htable.h"

void htab_foreach(htab_t *t, void (*fn)(char *key, int data))
{
	for (unsigned int i = 0; i < t->htab_size; i++)
	{
		if (t->ptr[i] == NULL) continue;
		
		fn(t->ptr[i]->key, t->ptr[i]->data);
	}
}

