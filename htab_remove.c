/*
	#FILENAME: 		htab_remove.c
	#DESCRIPTION:	Smazání položky se seznamu
	#Author:		Lukáš Chábek
*/
#include "htable.h"

void htab_remove(htab_t *t, const char *key)
{
	unsigned int index = hash_function(key, t->htab_size);
	
	if (t->ptr[index] == NULL)
		return;
		
	struct htab_listitem *helper = t->ptr[index];
	struct htab_listitem *item = NULL;
	
	while (helper != NULL)
	{
		if (strcmp(helper->key, key) == 0)
		{
			if (item == NULL && helper->next == NULL)
			{
				free(helper);
			}
			else if(item == NULL && helper->next != NULL)
			{
				item = helper;
				helper = helper->next;
				free(item);
			}
			else if(item != NULL && helper->next != NULL)
			{
				item->next = helper->next;
				free(helper);	
			}
			else if(item != NULL && helper->next == NULL)
			{
				free(item->next);
			}
		}
		item = helper;
		helper = helper->next;		
	}
	
}
