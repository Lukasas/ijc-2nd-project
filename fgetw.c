/*
	#FILENAME: 		fgetw.h
	#DESCRIPTION:	Čtení po slovech
	#Author:		Lukáš Chábek
*/
#include "fgetw.h"

int fgetw(char *s, int max, FILE *f)
{
	static char cErrPrinted = 0;
	char c;
	int counter = 0;
	if ((c = fgetc(f)) == '\n') return 0;
	
	for (; c == isspace(c); c = fgetc(f));
	if (c == '\n') return 0;
	if (c == EOF) return EOF;
	do
	{
		s[counter++] = c;
		if (counter >= max)
		{
			if (cErrPrinted == 0)
			{
				fprintf(stderr, "Byla překročena maximální délka slova %d.\n", max);
				cErrPrinted++;
			}
			s[counter] = '\0';
			
			for (; c != isspace(c) && c != '\n' && c != EOF; c = fgetc(f));
			return counter;
		}
	}
	while ((c = fgetc(f)) != '\n' && !isspace(c) && c != EOF);
	s[counter] = '\0';
	return counter;
}
