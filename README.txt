IJC 2 Project
Tail - C
./tail [-n[+]##] [file] < [file]
Reads input file or standard input and prints 10 last lines, or -n lines or prints from -n+# line.
Line limit is 510 chars per line.

Tail - C++
Same as Tail in C but without limit.

Wordcount - C
Counts words and store them in hash table.