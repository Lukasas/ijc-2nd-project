	COMPILER=gcc
CPP=g++
CPPFLAGS=-std=c++11 -pedantic -Wall
FLAGS=-std=c99 -pedantic -Wall -Wextra
OBJECTS=htab_statistics.o htab_remove.o htab_foreach.o htab_free.o htab_clear.o hlist_create.o htab_lookup.o htab_init.o hash_function.o


all: tail tail2 wordcount wordcount-dynamic

fgetw.o: fgetw.c
	$(COMPILER) $(FLAGS) -c $^ -o $@

wordcount: wordcount.o fgetw.o libhtable.a
	$(COMPILER) $(FLAGS) $^ -o $@

wordcount-dynamic: wordcount.o libhtable.so fgetw.o
	$(COMPILER) $(FLAGS)  $^ -o wordcount-dynamic

wordcount.o: wordcount.c
	$(COMPILER) $(FLAGS) -c $^ -o $@

tail_no_deal:
	$(CPP) -std=c++11 tail2.c -o tail2

tail.o: tail.c
	$(COMPILER) $(FLAGS) -c tail.c -o tail.o

tail: tail.o
	$(COMPILER) $(FLAGS) tail.o -o tail

tail2.o: tail2.cc
	$(CPP) $(CPPFLAGS) -c tail2.cc -o tail2.o

tail2: tail2.o
	$(CPP) $(CPPFLAGS) tail2.o -o tail2

hlist_create.o: hlist_create.c
	$(COMPILER) $(FLAGS) -fPIC -c $^ -o $@ 

libhtable.a: $(OBJECTS)
	ar crs libhtable.a $(OBJECTS)

libhtable.so: $(OBJECTS)
	$(COMPILER) $(FLAGS) -shared -fPIC $(OBJECTS) -o libhtable.so

hash_function.o: hash_function.c
	$(COMPILER) $(FLAGS) -fPIC -c $^ -o $@
#htab_
%.o: %.c
	$(COMPILER) $(FLAGS) -fPIC -c $<
 
clear:
	rm -f *.o
	rm -f tail
	rm -f tail2
	rm -f wordcount
	rm -f wordcount-dynamic
	rm -f *.a *.so
	rm -f xchabe00.zip

zip:
	zip xchabe00.zip *.c *.cc *.h Makefile
