/*
	#FILENAME: 		htab_free.c
	#DESCRIPTION:	Uvolnění naalokované hash tabulky.
	#Author:		Lukáš Chábek
*/
#include "htable.h"

void htab_free(htab_t *t)
{
	htab_clear(t);
	if (t != NULL)
		free(t);
}
