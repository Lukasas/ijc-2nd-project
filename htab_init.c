/*
	#FILENAME: 		htab_init.c
	#DESCRIPTION:	Vytvoření hashovací tabulky.
	#Author:		Lukáš Chábek
*/
#include "htable.h"

htab_t *htab_init(unsigned int htab_size)
{
	if (htab_size == 0)
		return NULL;
	
	htab_t * table = (htab_t*)malloc(sizeof(htab_t) + sizeof(struct htab_listitem) * htab_size);
	if (table == NULL)
		return NULL;
		
	table->htab_size = htab_size;
	
	for (unsigned int i = 0; i < htab_size; i++) table->ptr[i] = NULL;
	
	return table;
}
