/*
	#FILENAME: 		htab_statistics.c
	#DESCRIPTION:	Vytiskne průměrný počet položek v seznamu...
	#Author:		Lukáš Chábek
*/
#include "htable.h"

void htab_statistics(htab_t *t)
{
	int fullcounter = 0;	
	
	for (unsigned int i = 0; i < t->htab_size; i++)
	{
		if (t->ptr[i])
		fullcounter++;
	}
	printf("%d -- %0.20f\n", fullcounter, (float)fullcounter/(double)t->htab_size);
	  
}
