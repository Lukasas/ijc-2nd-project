/*
	#FILENAME: 		hlist_create.c
	#DESCRIPTION:	Vytvoření seznamu v tabulce.
	#Author:		Lukáš Chábek
*/
#include "htable.h"

struct htab_listitem * hlist_create(const char *key)
{
	struct htab_listitem * list = malloc(sizeof(struct htab_listitem)+sizeof(char)*(strlen(key)+1));
	if (list == NULL) return NULL;	
	strcpy(list->key, key);
	list->data = 0;
	list->next = NULL;
	return list;
}

