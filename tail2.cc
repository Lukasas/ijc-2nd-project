/*
	#FILENAME: 		tail2.cc
	#DESCRIPTION:	Vytiskne posledních/od X řádky/ů
	#Author:		Lukáš Chábek
*/
#include <iostream>
#include <string>
#include <queue>
#include <fstream>

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>

using namespace std;
void ReadEverything(istream *input, unsigned int numero = 10, bool from_line = false)
{
	string strBuffer;
	queue<string> myQueue;
	unsigned int line_count = (from_line ? 10 : numero);
	unsigned int counter = 0;
	while (getline(*input, strBuffer))
	{
		if (from_line)
		{
			counter++;
			if (counter >= numero)
				cout << strBuffer << endl;
		}
		else
			myQueue.push(strBuffer);
		
	}		
	
	if (from_line) return;
	/*	while (--numero)
			myQueue.pop();
		*/	
	while (!myQueue.empty())
	{
		if ((from_line == false && myQueue.size() <= line_count) || from_line == true)
			cout << myQueue.front() << endl;
		myQueue.pop();
	}
}

int main(int argc, char *argv[])
{

	char c;
	char * a = 0;
	int nvalue = 0;
	istream *defStream=&cin;
	ifstream fileStream;
	
	opterr = 0;
	/*
	if (argc == 1)
	{
		if (cin.tellg() != -1)
		{
			//vstup cin
			ReadEverything(defStream);
			return 0;
		}
		cerr << "Použití: tail [-n] [+][cislo] soubor." << endl;
		return 2;
	}
	*/
	while ((c = getopt(argc, argv, "n:")) != -1)
	{
		switch (c)
		{
			case 'n':
			a = optarg;
			nvalue = 1;
			break;
			case '?':
				if (optopt == 'n')
				{
					
					cerr << "Použití: tail [-n] [+][cislo] soubor." << endl;
					return 2;
				}
			break;
		}
	}
	
	for (int i = optind; i < argc; i++)
	{
		fileStream.open(argv[i], ios::in);
		if (!fileStream.is_open())
		{
			cerr << "Neznámý argument " << argv[i] << endl;
			return 2;
		}
		else
		{
			defStream = &fileStream;
			break;
		}
	}
/*
	if (defStream->tellg() == -1)
	{
		cerr << "Žádná vstupní data." << endl;
		return 3;
	}
*/
	ReadEverything(defStream, (nvalue == 1 ? abs(atoi(a)) : 10), (a == NULL ? false : a[0] == '+'));		
	fileStream.close();
	return 0;
}
