/*
	#FILENAME: 		htable.h
	#DESCRIPTION:	Hlavička knihovny LIBHTABLE.A / LIBHTABLE.SO
	#Author:		Lukáš Chábek
*/
#ifndef _HTABLE_IJC_PROJ_
#define _HTABLE_IJC_PROJ_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
struct htab_listitem
{
	unsigned int data;
	struct htab_listitem *next;
	char key[];
};

typedef struct
{
	unsigned int htab_size;
	struct htab_listitem *ptr[];
}htab_t;

void htab_remove(htab_t *t, const char *key);

void htab_foreach(htab_t *t, void (*fn)(char *key, int data));

void htab_free(htab_t *t);

void htab_clear(htab_t *t);

void htab_statistics(htab_t *t);

struct htab_listitem * hlist_create(const char *key);

struct htab_listitem * htab_lookup(htab_t *t, const char *key);

htab_t *htab_init(unsigned int htab_size);

unsigned int hash_function(const char *str, unsigned htab_size);
#endif
