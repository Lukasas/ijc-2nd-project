/*
	#FILENAME: 		wordcount.c
	#DESCRIPTION:	Spočítá stejné slova ze stdin.
	#Author:		Lukáš Chábek
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "fgetw.h"
#include "htable.h"

#define MAX_WORD 127

void my_printer(char *key, int data)
{
	printf("%s %d\n", key, data);
}


int main()
{
	htab_t *hTable = htab_init(100000);
	struct htab_listitem *item;
	if (!hTable)
	{
		fprintf(stderr,"Chyba při alokování paměti.\n");
		return 1;
	}
	
	int length = 0;
	char buffer[MAX_WORD+1] = { '\0' };
	while((length = fgetw(buffer, MAX_WORD, stdin)) != EOF)
	{
		if (!length) continue;
		
		if ((item = htab_lookup(hTable, buffer)) == NULL)
		{
			htab_free(hTable);
			return 2;
		}
		(item->data)++;
	}
	
	//htab_statistics(hTable);
	
	htab_foreach(hTable, my_printer);
	htab_free(hTable);
	
	return 0;
}
